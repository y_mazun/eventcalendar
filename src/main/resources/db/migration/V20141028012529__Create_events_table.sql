-- For H2 Database
create table events (
  id bigserial not null primary key,
  title varchar(512) not null,
  url varchar(512),
  event_at timestamp not null,
  created_at timestamp not null,
  updated_at timestamp not null
)
