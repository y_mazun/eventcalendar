-- For H2 Database
create table applications (
  id bigserial not null primary key,
  event_id bigint not null,
  url varchar(512) not null,
  start_at timestamp,
  end_at timestamp,
  confirm_at timestamp,
  deadline_at timestamp,
  created_at timestamp not null,
  updated_at timestamp not null
)
