package controller

import org.joda.time.DateTime
import skinny._
import skinny.validator._
import _root_.controller._
import model.Event

class EventsController extends SkinnyResource with ApplicationController {
  protectFromForgery()

  override def model = Event

  override def resourcesName = "events"

  override def resourceName = "event"

  override def resourcesBasePath = s"/${toSnakeCase(resourcesName)}"

  override def useSnakeCasedParamKeys = true

  override def viewsDirectoryPath = s"/${resourcesName}"

  override def createParams = Params(params).withDateTime("event_at")

  override def createForm = validation(createParams,
    paramKey("title") is required & maxLength(512),
    paramKey("url") is maxLength(512),
    paramKey("event_at") is required & dateTimeFormat
  )

  override def createFormStrongParameters = Seq(
    "title" -> ParamType.String,
    "url" -> ParamType.String,
    "event_at" -> ParamType.DateTime
  )

  override def updateParams = Params(params).withDateTime("event_at")

  override def updateForm = validation(updateParams,
    paramKey("title") is required & maxLength(512),
    paramKey("url") is maxLength(512),
    paramKey("event_at") is required & dateTimeFormat
  )

  override def updateFormStrongParameters = Seq(
    "title" -> ParamType.String,
    "url" -> ParamType.String,
    "event_at" -> ParamType.DateTime
  )

  override def showResources()(implicit format: Format = Format.HTML): Any = withFormat(format) {
    render(s"${viewsDirectoryPath}/index")
  }

  def month = (for {
    year <- params.getAs[Int]("year")
    month <- params.getAs[Int]("month")
  } yield {
    val start = new DateTime(year, month, 1, 0, 0)
    val end = new DateTime(year, month, start.dayOfMonth().getMaximumValue(), 23, 59)
    val events = Event.findAllBetween(start, end)
    toJSON(events)
  }) getOrElse haltWithBody(404)
}
