package controller

import skinny._
import skinny.validator._
import _root_.controller._
import model.Application

class ApplicationsController extends SkinnyResource with ApplicationController {
  protectFromForgery()

  override def model = Application
  override def resourcesName = "applications"
  override def resourceName = "application"

  override def resourcesBasePath = s"/${toSnakeCase(resourcesName)}"
  override def useSnakeCasedParamKeys = true

  override def viewsDirectoryPath = s"/${resourcesName}"

  override def createParams = Params(params).withDateTime("start_at").withDateTime("end_at").withDateTime("confirm_at").withDateTime("deadline_at")
  override def createForm = validation(createParams,
    paramKey("event_id") is required & numeric & longValue,
    paramKey("url") is required & maxLength(512),
    paramKey("start_at") is dateTimeFormat,
    paramKey("end_at") is dateTimeFormat,
    paramKey("confirm_at") is dateTimeFormat,
    paramKey("deadline_at") is dateTimeFormat
  )
  override def createFormStrongParameters = Seq(
    "event_id" -> ParamType.Long,
    "url" -> ParamType.String,
    "start_at" -> ParamType.DateTime,
    "end_at" -> ParamType.DateTime,
    "confirm_at" -> ParamType.DateTime,
    "deadline_at" -> ParamType.DateTime
  )

  override def updateParams = Params(params).withDateTime("start_at").withDateTime("end_at").withDateTime("confirm_at").withDateTime("deadline_at")
  override def updateForm = validation(updateParams,
    paramKey("event_id") is required & numeric & longValue,
    paramKey("url") is required & maxLength(512),
    paramKey("start_at") is dateTimeFormat,
    paramKey("end_at") is dateTimeFormat,
    paramKey("confirm_at") is dateTimeFormat,
    paramKey("deadline_at") is dateTimeFormat
  )
  override def updateFormStrongParameters = Seq(
    "event_id" -> ParamType.Long,
    "url" -> ParamType.String,
    "start_at" -> ParamType.DateTime,
    "end_at" -> ParamType.DateTime,
    "confirm_at" -> ParamType.DateTime,
    "deadline_at" -> ParamType.DateTime
  )

  def event = (for{
    id <- params.getAs[Long]("id")
  } yield {
    toJSON(Application.findAllByEventId(id))
  }) getOrElse haltWithBody(404)
}
