package controller

import skinny._
import skinny.controller.AssetsController

object Controllers {

  def mount(ctx: ServletContext): Unit = {
    applications.mount(ctx)
    events.mount(ctx)
    root.mount(ctx)
    AssetsController.mount(ctx)
  }

  object root extends RootController with Routes {
    val indexUrl = get("/?")(index).as('index)
  }
  object events extends _root_.controller.EventsController with Routes {
    val monthUrl = get("/api/events/month/:year/:month")(month).as('month)
  }
  object applications extends _root_.controller.ApplicationsController with Routes {
    val eventsUrl = get("/api/applications/:id")(event).as('event)
  }

}
