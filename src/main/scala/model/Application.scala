package model

import skinny.orm._, feature._
import scalikejdbc._
import org.joda.time._

// If your model has +23 fields, switch this to normal class and mixin scalikejdbc.EntityEquality.
case class Application(
  id: Long,
  eventId: Long,
  url: String,
  startAt: Option[DateTime] = None,
  endAt: Option[DateTime] = None,
  confirmAt: Option[DateTime] = None,
  deadlineAt: Option[DateTime] = None,
  createdAt: DateTime,
  updatedAt: DateTime
)

object Application extends SkinnyCRUDMapper[Application] with TimestampsFeature[Application] {
  override lazy val tableName = "applications"
  override lazy val defaultAlias = createAlias("a")

  /*
   * If you're familiar with ScalikeJDBC/Skinny ORM, using #autoConstruct makes your mapper simpler.
   * (e.g.)
   * override def extract(rs: WrappedResultSet, rn: ResultName[Application]) = autoConstruct(rs, rn)
   *
   * Be aware of excluding associations like this:
   * (e.g.)
   * case class Member(id: Long, companyId: Long, company: Option[Company] = None)
   * object Member extends SkinnyCRUDMapper[Member] {
   *   override def extract(rs: WrappedResultSet, rn: ResultName[Member]) =
   *     autoConstruct(rs, rn, "company") // "company" will be skipped
   * }
   */
  override def extract(rs: WrappedResultSet, rn: ResultName[Application]): Application = new Application(
    id = rs.get(rn.id),
    eventId = rs.get(rn.eventId),
    url = rs.get(rn.url),
    startAt = rs.get(rn.startAt),
    endAt = rs.get(rn.endAt),
    confirmAt = rs.get(rn.confirmAt),
    deadlineAt = rs.get(rn.deadlineAt),
    createdAt = rs.get(rn.createdAt),
    updatedAt = rs.get(rn.updatedAt)
  )

  def findAllByEventId(id: Long) = findAllBy(sqls.eq(Application.column.eventId, id))
}
