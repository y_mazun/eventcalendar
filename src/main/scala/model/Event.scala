package model

import skinny.orm._, feature._
import scalikejdbc._
import org.joda.time._

// If your model has +23 fields, switch this to normal class and mixin scalikejdbc.EntityEquality.
case class Event(
  id: Long,
  title: String,
  url: Option[String] = None,
  eventAt: DateTime,
  createdAt: DateTime,
  updatedAt: DateTime
)

object Event extends SkinnyCRUDMapper[Event] with TimestampsFeature[Event] {
  override lazy val tableName = "events"
  override lazy val defaultAlias = createAlias("e")

  /*
   * If you're familiar with ScalikeJDBC/Skinny ORM, using #autoConstruct makes your mapper simpler.
   * (e.g.)
   * override def extract(rs: WrappedResultSet, rn: ResultName[Event]) = autoConstruct(rs, rn)
   *
   * Be aware of excluding associations like this:
   * (e.g.)
   * case class Member(id: Long, companyId: Long, company: Option[Company] = None)
   * object Member extends SkinnyCRUDMapper[Member] {
   *   override def extract(rs: WrappedResultSet, rn: ResultName[Member]) =
   *     autoConstruct(rs, rn, "company") // "company" will be skipped
   * }
   */
  override def extract(rs: WrappedResultSet, rn: ResultName[Event]): Event = new Event(
    id = rs.get(rn.id),
    title = rs.get(rn.title),
    url = rs.get(rn.url),
    eventAt = rs.get(rn.eventAt),
    createdAt = rs.get(rn.createdAt),
    updatedAt = rs.get(rn.updatedAt)
  )

  def findAllBetween(start: DateTime, end: DateTime) = findAllBy(sqls.between(Event.column.eventAt, start, end))
}
