/// <reference path='tools/jquery/jquery.d.ts' />

module EventCalendar {
    export class Application {
        id: number
        eventId: number
        url: string
        startAt: Date
        endAt: Date
        confirmAt: Date
        deadlineAt: Date

        private static getUrl: string = "/api/applications/"

        static getByEvent(id: number): JQueryPromise<Application[]> {
            var dfr = $.Deferred();
            $.getJSON(this.getUrl + id, dfr.resolve);
            return dfr.promise();
        }
    }
}
