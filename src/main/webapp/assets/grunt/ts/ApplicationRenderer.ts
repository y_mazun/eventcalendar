/// <reference path='tools/jquery/jquery.d.ts' />
/// <reference path='tools/director/director.d.ts' />
/// <reference path='tools/vue/vue.0.10.6.d.ts' />

module EventCalendar {
    export class ApplicationRenderer {
        vue: any;

        constructor(private selector: string) {
            this.vue = new Vue({
                el: this.selector,
                data: {
                    applications: []
                }
            });
        }

        render(id: number) {
            Application.getByEvent(id).then(applications => {
                console.log(applications);
                this.vue.$data.applications = applications;
            });
        }
    }
}
