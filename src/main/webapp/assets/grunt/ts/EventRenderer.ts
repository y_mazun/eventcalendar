/// <reference path='tools/jquery/jquery.d.ts' />
/// <reference path='tools/director/director.d.ts' />
/// <reference path='tools/vue/vue.0.10.6.d.ts' />

module EventCalendar {
    export class EventRenderer{
        vue: any;

        constructor(private selector: string) {
            this.vue = new Vue({
                el: this.selector,
                data: {
                    searchText: "",
                    year: "",
                    month: "",
                    events: []
                },
                methods: {
                    toNext: () => this.nextMonth(),
                    toPrev: () => this.prevMonth()
                }
            });
        }

        renderByUrl(): void {
            var hash: string = location.hash.substr(1);
            extractMatch(hash, /\d{6}/).match({
                Some: (x) => {
                    var year = Number(x.substr(0, 4));
                    var month = Number(x.substr(4, 2));
                    if (0 <= year && 1 <= month && month <= 12)
                        this.render(year, month);
                    else
                        this.renderToday();
                },
                None: () => this.renderToday()
            });
        }

        renderToday(): void {
            var today = new Date();
            this.render(today.getFullYear(), today.getMonth() + 1);
        }

        render(year: number, month: number): void {
            var v = this.vue;
            v.$data.year = year;
            v.$data.month = month;
            location.hash = year + "" + (month < 10 ? "0" + month : String(month));
            Event.getSpecificMonthEvents(year, month).then(events => {
                v.$data.events = events;
            });
        }

        private nextMonth(): void {
            var month = this.vue.$data.month;
            var year = this.vue.$data.year;
            if (month == 12) {
                month = 1;
                year++;
            } else {
                month++;
            }
            this.render(year, month);
        }

        private prevMonth(): void {
            var month = this.vue.$data.month;
            var year = this.vue.$data.year;
            if (month == 1) {
                month = 12;
                year--;
            } else {
                month--;
            }
            this.render(year, month);
        }
    }
}
