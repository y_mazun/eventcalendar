interface IOptionMatcher<T> {
    Some? (value: T): void;
    None? (): void
}

interface Option<T> {
    get(): T;
    getOrElse(defaultValue: T): T;
    match(matcher: IOptionMatcher<T>);
    map<S>(f: (value: T) => S): Option<S>;
    flatMap<S>(f: (value: T) => Option<S>): Option<S>;
}

function some<T>(value: T): Option<T> {
    return new Some<T>(value);
}

function none() {
    return new None<any>();
}

class Some<T> implements Option<T>{
    constructor(private value: T) {
    }

    get(): T {
        return this.value;
    }

    getOrElse(defaultValue: T): T {
        return this.value;
    }

    match(matcher: IOptionMatcher<T>) {
        if (matcher.Some) {
            matcher.Some(this.value);
        }
    }

    map<S>(f: (value: T) => S): Option<S> {
        return new Some<S>(f(this.value));
    }

    flatMap<S>(f: (value: T) => Option<S>): Option<S> {
        return f(this.value);
    }
}

class None<T> implements Option<T>{
    constructor() {
    }

    get(): T {
        throw "Cannot get value from None";
    }

    getOrElse(defaultValue: T): T {
        return defaultValue;
    }

    match(matcher: IOptionMatcher<T>) {
        if (matcher.None) {
            matcher.None()
        }
    }

    map<S>(f: (value: T) => S): Option<S> {
        return new None<S>();
    }

    flatMap<S>(f: (value: T) => Option<S>): Option<S> {
        return new None<S>();
    }
}
