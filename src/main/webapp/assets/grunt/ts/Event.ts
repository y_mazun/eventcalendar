/// <reference path='tools/jquery/jquery.d.ts' />

module EventCalendar {
    export class Event {
        id: number
        title: string
        url: string
        event_at: Date

        public static getMonthUrl: string = "/api/events/month/"
        public static showUrl: string = "/events/"
        private static cache = {}

        getShowUrl(): string {
            return Event.showUrl + this.id;
        }

        static getSpecificMonthEvents(year: number, month: number): JQueryPromise<Event[]> {
            var dfr = $.Deferred();
            var key = year + "/" + month;
            if (key in this.cache) {
                dfr.resolve(this.cache[key]);
            } else {
                $.getJSON(this.getMonthUrl + year + "/" + month, (e) => {
                    this.cache[key] = e;
                    dfr.resolve(e);
                });
            }
            return dfr.promise();
        }
    }

    Vue.filter('eventShowUrl', (id: number) => {
        return Event.showUrl + id;
    });
}
