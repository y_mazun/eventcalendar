/// <reference path='tools/vue/vue.0.10.6.d.ts' />

module EventCalendar {
    export function extractMatch(value: string, regexp: RegExp): Option<string> {
        if (!value) return none();
        var r = value.match(regexp);
        if (r) return some(r[0]);
        else return none();
    }

    Vue.filter('onlyDate', (value: string) => {
        return extractMatch(value, /\d+-\d+-\d+/).getOrElse("");
    });
}