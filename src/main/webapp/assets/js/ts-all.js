var EventCalendar;
(function (EventCalendar) {
    var Application = (function () {
        function Application() {
        }
        Application.getByEvent = function (id) {
            var dfr = $.Deferred();
            $.getJSON(this.getUrl + id, dfr.resolve);
            return dfr.promise();
        };
        Application.getUrl = "/api/applications/";
        return Application;
    })();
    EventCalendar.Application = Application;
})(EventCalendar || (EventCalendar = {}));
var EventCalendar;
(function (EventCalendar) {
    var ApplicationRenderer = (function () {
        function ApplicationRenderer(selector) {
            this.selector = selector;
            this.vue = new Vue({
                el: this.selector,
                data: {
                    applications: []
                }
            });
        }
        ApplicationRenderer.prototype.render = function (id) {
            var _this = this;
            EventCalendar.Application.getByEvent(id).then(function (applications) {
                console.log(applications);
                _this.vue.$data.applications = applications;
            });
        };
        return ApplicationRenderer;
    })();
    EventCalendar.ApplicationRenderer = ApplicationRenderer;
})(EventCalendar || (EventCalendar = {}));
var EventCalendar;
(function (EventCalendar) {
    var Event = (function () {
        function Event() {
        }
        Event.prototype.getShowUrl = function () {
            return Event.showUrl + this.id;
        };

        Event.getSpecificMonthEvents = function (year, month) {
            var _this = this;
            var dfr = $.Deferred();
            var key = year + "/" + month;
            if (key in this.cache) {
                dfr.resolve(this.cache[key]);
            } else {
                $.getJSON(this.getMonthUrl + year + "/" + month, function (e) {
                    _this.cache[key] = e;
                    dfr.resolve(e);
                });
            }
            return dfr.promise();
        };
        Event.getMonthUrl = "/api/events/month/";
        Event.showUrl = "/events/";
        Event.cache = {};
        return Event;
    })();
    EventCalendar.Event = Event;

    Vue.filter('eventShowUrl', function (id) {
        return Event.showUrl + id;
    });
})(EventCalendar || (EventCalendar = {}));
var EventCalendar;
(function (EventCalendar) {
    var EventRenderer = (function () {
        function EventRenderer(selector) {
            var _this = this;
            this.selector = selector;
            this.vue = new Vue({
                el: this.selector,
                data: {
                    searchText: "",
                    year: "",
                    month: "",
                    events: []
                },
                methods: {
                    toNext: function () {
                        return _this.nextMonth();
                    },
                    toPrev: function () {
                        return _this.prevMonth();
                    }
                }
            });
        }
        EventRenderer.prototype.renderByUrl = function () {
            var _this = this;
            var hash = location.hash.substr(1);
            EventCalendar.extractMatch(hash, /\d{6}/).match({
                Some: function (x) {
                    var year = Number(x.substr(0, 4));
                    var month = Number(x.substr(4, 2));
                    if (0 <= year && 1 <= month && month <= 12)
                        _this.render(year, month);
                    else
                        _this.renderToday();
                },
                None: function () {
                    return _this.renderToday();
                }
            });
        };

        EventRenderer.prototype.renderToday = function () {
            var today = new Date();
            this.render(today.getFullYear(), today.getMonth() + 1);
        };

        EventRenderer.prototype.render = function (year, month) {
            var v = this.vue;
            v.$data.year = year;
            v.$data.month = month;
            location.hash = year + "" + (month < 10 ? "0" + month : String(month));
            EventCalendar.Event.getSpecificMonthEvents(year, month).then(function (events) {
                v.$data.events = events;
            });
        };

        EventRenderer.prototype.nextMonth = function () {
            var month = this.vue.$data.month;
            var year = this.vue.$data.year;
            if (month == 12) {
                month = 1;
                year++;
            } else {
                month++;
            }
            this.render(year, month);
        };

        EventRenderer.prototype.prevMonth = function () {
            var month = this.vue.$data.month;
            var year = this.vue.$data.year;
            if (month == 1) {
                month = 12;
                year--;
            } else {
                month--;
            }
            this.render(year, month);
        };
        return EventRenderer;
    })();
    EventCalendar.EventRenderer = EventRenderer;
})(EventCalendar || (EventCalendar = {}));
var EventCalendar;
(function (EventCalendar) {
    function extractMatch(value, regexp) {
        if (!value)
            return none();
        var r = value.match(regexp);
        if (r)
            return some(r[0]);
        else
            return none();
    }
    EventCalendar.extractMatch = extractMatch;

    Vue.filter('onlyDate', function (value) {
        return extractMatch(value, /\d+-\d+-\d+/).getOrElse("");
    });
})(EventCalendar || (EventCalendar = {}));
function some(value) {
    return new Some(value);
}

function none() {
    return new None();
}

var Some = (function () {
    function Some(value) {
        this.value = value;
    }
    Some.prototype.get = function () {
        return this.value;
    };

    Some.prototype.getOrElse = function (defaultValue) {
        return this.value;
    };

    Some.prototype.match = function (matcher) {
        if (matcher.Some) {
            matcher.Some(this.value);
        }
    };

    Some.prototype.map = function (f) {
        return new Some(f(this.value));
    };

    Some.prototype.flatMap = function (f) {
        return f(this.value);
    };
    return Some;
})();

var None = (function () {
    function None() {
    }
    None.prototype.get = function () {
        throw "Cannot get value from None";
    };

    None.prototype.getOrElse = function (defaultValue) {
        return defaultValue;
    };

    None.prototype.match = function (matcher) {
        if (matcher.None) {
            matcher.None();
        }
    };

    None.prototype.map = function (f) {
        return new None();
    };

    None.prototype.flatMap = function (f) {
        return new None();
    };
    return None;
})();
//# sourceMappingURL=ts-all.js.map
