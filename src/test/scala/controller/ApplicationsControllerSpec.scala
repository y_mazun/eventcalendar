package controller

import org.scalatest._
import skinny._
import skinny.test._
import org.joda.time._
import model._

// NOTICE before/after filters won't be executed by default
class ApplicationsControllerSpec extends FunSpec with Matchers with BeforeAndAfterAll with DBSettings {

  override def afterAll() {
    super.afterAll()
    Application.deleteAll()
  }

  def createMockController = new ApplicationsController with MockController
  def newApplication = FactoryGirl(Application).create()

  describe("ApplicationsController") {

    describe("shows applications") {
      it("shows HTML response") {
        val controller = createMockController
        controller.showResources()
        controller.status should equal(200)
        controller.renderCall.map(_.path) should equal(Some("/applications/index"))
        controller.contentType should equal("text/html; charset=utf-8")
      }

      it("shows JSON response") {
        implicit val format = Format.JSON
        val controller = createMockController
        controller.showResources()
        controller.status should equal(200)
        controller.renderCall.map(_.path) should equal(Some("/applications/index"))
        controller.contentType should equal("application/json; charset=utf-8")
      }
    }

    describe("shows a application") {
      it("shows HTML response") {
        val application = newApplication
        val controller = createMockController
        controller.showResource(application.id)
        controller.status should equal(200)
        controller.getFromRequestScope[Application]("item") should equal(Some(application))
        controller.renderCall.map(_.path) should equal(Some("/applications/show"))
      }
    }

    describe("shows new resource input form") {
      it("shows HTML response") {
        val controller = createMockController
        controller.newResource()
        controller.status should equal(200)
        controller.renderCall.map(_.path) should equal(Some("/applications/new"))
      }
    }

    describe("creates a application") {
      it("succeeds with valid parameters") {
        val controller = createMockController
        controller.prepareParams(
          "event_id" -> Long.MaxValue.toString(),
          "url" -> "dummy",
          "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
          "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
          "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
          "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime()))
        controller.createResource()
        controller.status should equal(200)
      }

      it("fails with invalid parameters") {
        val controller = createMockController
        controller.prepareParams() // no parameters
        controller.createResource()
        controller.status should equal(400)
        controller.errorMessages.size should be >(0)
      }
    }

    it("shows a resource edit input form") {
      val application = newApplication
      val controller = createMockController
      controller.editResource(application.id)
      controller.status should equal(200)
        controller.renderCall.map(_.path) should equal(Some("/applications/edit"))
    }

    it("updates a application") {
      val application = newApplication
      val controller = createMockController
      controller.prepareParams(
        "event_id" -> Long.MaxValue.toString(),
        "url" -> "dummy",
        "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime()))
      controller.updateResource(application.id)
      controller.status should equal(200)
    }

    it("destroys a application") {
      val application = newApplication
      val controller = createMockController
      controller.destroyResource(application.id)
      controller.status should equal(200)
    }

  }

}
