package integrationtest

import org.scalatra.test.scalatest._
import org.scalatest._
import skinny._
import skinny.test._
import org.joda.time._
import _root_.controller.Controllers
import model._

class ApplicationsController_IntegrationTestSpec extends ScalatraFlatSpec with SkinnyTestSupport with BeforeAndAfterAll with DBSettings {
  addFilter(Controllers.applications, "/*")

  override def afterAll() {
    super.afterAll()
    Application.deleteAll()
  }

  def newApplication = FactoryGirl(Application).create()

  it should "show applications" in {
    get("/applications") {
      logBodyUnless(200)
      status should equal(200)
    }
    get("/applications/") {
      logBodyUnless(200)
      status should equal(200)
    }
    get("/applications.json") {
      logBodyUnless(200)
      status should equal(200)
    }
    get("/applications.xml") {
      logBodyUnless(200)
      status should equal(200)
    }
  }

  it should "show a application in detail" in {
    get(s"/applications/${newApplication.id}") {
      logBodyUnless(200)
      status should equal(200)
    }
    get(s"/applications/${newApplication.id}.xml") {
      logBodyUnless(200)
      status should equal(200)
    }
    get(s"/applications/${newApplication.id}.json") {
      logBodyUnless(200)
      status should equal(200)
    }
  }

  it should "show new entry form" in {
    get(s"/applications/new") {
      logBodyUnless(200)
      status should equal(200)
    }
  }

  it should "create a application" in {
    post(s"/applications",
      "event_id" -> Long.MaxValue.toString(),
      "url" -> "dummy",
      "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime())) {
      logBodyUnless(403)
      status should equal(403)
    }

    withSession("csrf-token" -> "valid_token") {
      post(s"/applications",
        "event_id" -> Long.MaxValue.toString(),
        "url" -> "dummy",
        "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "csrf-token" -> "valid_token") {
        logBodyUnless(302)
        status should equal(302)
        val id = header("Location").split("/").last.toLong
        Application.findById(id).isDefined should equal(true)
      }
    }
  }

  it should "show the edit form" in {
    get(s"/applications/${newApplication.id}/edit") {
      logBodyUnless(200)
      status should equal(200)
    }
  }

  it should "update a application" in {
    put(s"/applications/${newApplication.id}",
      "event_id" -> Long.MaxValue.toString(),
      "url" -> "dummy",
      "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
      "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime())) {
      logBodyUnless(403)
      status should equal(403)
    }

    withSession("csrf-token" -> "valid_token") {
      put(s"/applications/${newApplication.id}",
        "event_id" -> Long.MaxValue.toString(),
        "url" -> "dummy",
        "start_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "end_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "confirm_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "deadline_at" -> skinny.util.DateTimeUtil.toString(new DateTime()),
        "csrf-token" -> "valid_token") {
        logBodyUnless(302)
        status should equal(302)
      }
    }
  }

  it should "delete a application" in {
    delete(s"/applications/${newApplication.id}") {
      logBodyUnless(403)
      status should equal(403)
    }
    withSession("csrf-token" -> "valid_token") {
      delete(s"/applications/${newApplication.id}?csrf-token=valid_token") {
        logBodyUnless(200)
        status should equal(200)
      }
    }
  }

}
