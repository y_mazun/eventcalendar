# 支払い忘れをしてはならない #

### 実行方法
    git clone git@bitbucket.org:y_mazun/eventcalendar.git
    cd eventcalendar
    ./skinny db:migrate
    ./skinny run

起動したら localhost:8080/events にブラウザでアクセスしてみてください。

### 開発者向け
以下でtypescriptとlessが自動的にコンパイルされます。
それぞれのコンパイラは事前にインストールしてください。

    (sudo) npm install
    (sudo) npm install -g grunt-cli
    grunt

### Skinny Framework

http://skinny-framework.org/